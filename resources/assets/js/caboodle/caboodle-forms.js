$(document).ready(function() {
   /* CABOODLE CARD ENABLE TARGETING */
   $('.caboodle-card-enable-target').on('click', function(e) {
        e.preventDefault();
        var target = $(this).data('target');
        var status = $(this).data('enabled');
        var el = $(target);
        $(this).closest('.caboodle-card').find('[data-enabled="true"]').toggleClass('hide', status);
        $(target).toggleClass('hide', !status);
        var textareas = $(this).closest('.caboodle-card').find('textarea').attr('readonly', !status);
        var inputs = $(this).closest('.caboodle-card').find('input').attr('readonly', !status);
        var selects = $(this).closest('.caboodle-card').find('select').attr('disabled', !status);
        var checkboxes = $(this).closest('.caboodle-card').find('input[type="checkbox"]').attr('disabled', !status);
        try {
            textareas[0].focus();
            SetCaretAtEnd(textareas[0]);
        } catch(e) {}
        try {
            inputs[0].focus();
            SetCaretAtEnd(inputs[0]);
        } catch(e) {}
        
        /* Custom callback function */
        try {
            caboodleEnableTargetCallback($(this).closest('.caboodle-card'), status);
        } catch(e) {}
    });

    $('.reset-card').on('click', function(e) {
        $(this).closest('form').find('input').each(function(e) {
            $(this).val($(this).attr('orig-value'));
        });
        $(this).closest('form').find('input[type="checkbox"]').each(function (e) {
            var origVal = $(this).attr('orig-value');
            $(this).prop('checked', (origVal == 1 || origVal == '1') ? true : false);
        });
        $(this).closest('form').find('textarea').each(function (e) {
            $(this).html($(this).attr('orig-value'));
        });
        $(this).closest('form').find('select').each(function (e) {
            $(this).val($(this).attr('orig-value'));
        });
    });
    /* CABOODLE ENABLE TARGETING -- END*/

    /* CABOODLE SUBMIT CARD FORM  */
    
    /* CABOODLE SUBMIT CARD FORM -- END */
    
    $('.seo-url').each(function () {
        var url = $(this).data('url');
        $(this).find('.form-seo').attr('action', url);
    });
});