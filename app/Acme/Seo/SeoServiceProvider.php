<?php

namespace App\Acme\Seo;

use Illuminate\Support\ServiceProvider;

class SeoServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('seo', 'App\Acme\Seo\Seo');
	}
}